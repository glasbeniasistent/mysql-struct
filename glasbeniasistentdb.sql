-- MySQL Script generated by MySQL Workbench
-- pet 29 nov 2019 14:37:43 CET
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema glasbeniasistentdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema glasbeniasistentdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS glasbeniasistentdb ;
USE glasbeniasistentdb ;

-- -----------------------------------------------------
-- Table glasbeniasistentdb.types_of_bands
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.types_of_bands ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.types_of_bands (
  type_of_band_id INT NOT NULL AUTO_INCREMENT,
  type VARCHAR(100) NOT NULL,
  PRIMARY KEY (type_of_band_id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.citys
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.citys ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.citys (
  city_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  PRIMARY KEY (city_id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.countrys
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.countrys ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.countrys (
  country_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  PRIMARY KEY (country_id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.addresses
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.addresses ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.addresses (
  address_id INT NOT NULL AUTO_INCREMENT,
  city_id INT NOT NULL,
  country_id INT NOT NULL,
  address VARCHAR(200) NOT NULL,
  PRIMARY KEY (address_id, city_id, country_id),
  INDEX fk_addresses_citys1_idx (city_id ASC),
  INDEX fk_addresses_countrys1_idx (country_id ASC),
  CONSTRAINT fk_addresses_citys1
    FOREIGN KEY (city_id)
    REFERENCES glasbeniasistentdb.citys (city_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_addresses_countrys1
    FOREIGN KEY (country_id)
    REFERENCES glasbeniasistentdb.countrys (country_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.bands
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.bands ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.bands (
  band_id INT NOT NULL AUTO_INCREMENT,
  type_of_band_id INT NOT NULL,
  address_id INT NOT NULL,
  band_name VARCHAR(50) NOT NULL,
  premium TINYINT NOT NULL DEFAULT 0,
  description TEXT(500) NOT NULL,
  PRIMARY KEY (band_id, type_of_band_id, address_id),
  INDEX fk_bands_types_of_bands1_idx (type_of_band_id ASC),
  INDEX fk_bands_addresses1_idx (address_id ASC),
  CONSTRAINT fk_bands_types_of_bands1
    FOREIGN KEY (type_of_band_id)
    REFERENCES glasbeniasistentdb.types_of_bands (type_of_band_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_bands_addresses1
    FOREIGN KEY (address_id)
    REFERENCES glasbeniasistentdb.addresses (address_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.users
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.users ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.users (
  user_id INT NOT NULL AUTO_INCREMENT,
  band_id INT NOT NULL,
  name VARCHAR(50) NOT NULL,
  surename VARCHAR(50) NOT NULL,
  email VARCHAR(60) NOT NULL,
  password VARCHAR(400) NOT NULL,
  premium TINYINT NOT NULL DEFAULT 0,
  sec_token VARCHAR(100) NOT NULL DEFAULT 'nothing',
  PRIMARY KEY (user_id, band_id),
  INDEX fk_users_bands_idx (band_id ASC),
  CONSTRAINT fk_users_bands
    FOREIGN KEY (band_id)
    REFERENCES glasbeniasistentdb.bands (band_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.perfomances
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.perfomances ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.perfomances (
  performance_id INT NOT NULL AUTO_INCREMENT,
  band_id INT NOT NULL,
  address_id INT NOT NULL,
  date DATE NOT NULL,
  time TIME NOT NULL,
  description TEXT(1000) NULL DEFAULT NULL,
  PRIMARY KEY (performance_id, band_id, address_id),
  INDEX fk_perfomances_bands1_idx (band_id ASC),
  INDEX fk_perfomances_addresses1_idx (address_id ASC),
  CONSTRAINT fk_perfomances_bands1
    FOREIGN KEY (band_id)
    REFERENCES glasbeniasistentdb.bands (band_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_perfomances_addresses1
    FOREIGN KEY (address_id)
    REFERENCES glasbeniasistentdb.addresses (address_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.artists
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.artists ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.artists (
  artist_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  PRIMARY KEY (artist_id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.songs
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.songs ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.songs (
  song_id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(100) NOT NULL,
  lyrics TEXT(20000) NOT NULL,
  original TINYINT NOT NULL DEFAULT 1,
  order_of_parts VARCHAR(100) NULL,
  accepted TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (song_id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.band_songs
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.band_songs ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.band_songs (
  band_song_id INT NOT NULL AUTO_INCREMENT,
  song_id INT NOT NULL,
  order_number INT NULL DEFAULT NULL,
  tunning VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (band_song_id, song_id),
  INDEX fk_band_songs_songs1_idx (song_id ASC),
  CONSTRAINT fk_band_songs_songs1
    FOREIGN KEY (song_id)
    REFERENCES glasbeniasistentdb.songs (song_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.types_of_songs
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.types_of_songs ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.types_of_songs (
  type_of_song_id INT NOT NULL AUTO_INCREMENT,
  type VARCHAR(100) NOT NULL,
  PRIMARY KEY (type_of_song_id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.practices
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.practices ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.practices (
  practice_id INT NOT NULL AUTO_INCREMENT,
  band_id INT NOT NULL,
  address_id INT NOT NULL,
  date DATE NOT NULL,
  time TIME NOT NULL,
  PRIMARY KEY (practice_id, band_id, address_id),
  INDEX fk_practices_bands1_idx (band_id ASC),
  INDEX fk_practices_addresses1_idx (address_id ASC),
  CONSTRAINT fk_practices_bands1
    FOREIGN KEY (band_id)
    REFERENCES glasbeniasistentdb.bands (band_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_practices_addresses1
    FOREIGN KEY (address_id)
    REFERENCES glasbeniasistentdb.addresses (address_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.practices_songs
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.practices_songs ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.practices_songs (
  practice_song_id INT NOT NULL AUTO_INCREMENT,
  band_song_id INT NOT NULL,
  practice_id INT NOT NULL,
  PRIMARY KEY (practice_song_id, band_song_id, practice_id),
  INDEX fk_practices_songs_band_songs1_idx (band_song_id ASC),
  INDEX fk_practices_songs_practices1_idx (practice_id ASC),
  CONSTRAINT fk_practices_songs_band_songs1
    FOREIGN KEY (band_song_id)
    REFERENCES glasbeniasistentdb.band_songs (band_song_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_practices_songs_practices1
    FOREIGN KEY (practice_id)
    REFERENCES glasbeniasistentdb.practices (practice_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.artists_has_songs
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.artists_has_songs ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.artists_has_songs (
  artist_has_song_id INT NOT NULL AUTO_INCREMENT,
  artist_id INT NOT NULL,
  song_id INT NOT NULL,
  PRIMARY KEY (artist_has_song_id, artist_id, song_id),
  INDEX fk_artists_has_songs_songs1_idx (song_id ASC),
  INDEX fk_artists_has_songs_artists1_idx (artist_id ASC),
  CONSTRAINT fk_artists_has_songs_artists1
    FOREIGN KEY (artist_id)
    REFERENCES glasbeniasistentdb.artists (artist_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_artists_has_songs_songs1
    FOREIGN KEY (song_id)
    REFERENCES glasbeniasistentdb.songs (song_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.types_of_songs_has_songs
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.types_of_songs_has_songs ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.types_of_songs_has_songs (
  type_of_song_has_song_id INT NOT NULL AUTO_INCREMENT,
  type_of_song_id INT NOT NULL,
  song_id INT NOT NULL,
  PRIMARY KEY (type_of_song_has_song_id, type_of_song_id, song_id),
  INDEX fk_types_of_songs_has_songs_songs1_idx (song_id ASC),
  INDEX fk_types_of_songs_has_songs_types_of_songs1_idx (type_of_song_id ASC),
  CONSTRAINT fk_types_of_songs_has_songs_types_of_songs1
    FOREIGN KEY (type_of_song_id)
    REFERENCES glasbeniasistentdb.types_of_songs (type_of_song_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_types_of_songs_has_songs_songs1
    FOREIGN KEY (song_id)
    REFERENCES glasbeniasistentdb.songs (song_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.maintenance
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.maintenance ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.maintenance (
  user_id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(45) NOT NULL,
  password VARCHAR(400) NOT NULL,
  status VARCHAR(45) NOT NULL,
  PRIMARY KEY (user_id))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table glasbeniasistentdb.band_has_songs
-- -----------------------------------------------------
DROP TABLE IF EXISTS glasbeniasistentdb.band_has_songs ;

CREATE TABLE IF NOT EXISTS glasbeniasistentdb.band_has_songs (
  band_has_song INT NOT NULL AUTO_INCREMENT,
  band_song_id INT NOT NULL,
  band_id INT NOT NULL,
  PRIMARY KEY (band_has_song, band_song_id, band_id),
  INDEX fk_band_has_songs_band_songs1_idx (band_song_id ASC),
  INDEX fk_band_has_songs_bands1_idx (band_id ASC),
  CONSTRAINT fk_band_has_songs_band_songs1
    FOREIGN KEY (band_song_id)
    REFERENCES glasbeniasistentdb.band_songs (band_song_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_band_has_songs_bands1
    FOREIGN KEY (band_id)
    REFERENCES glasbeniasistentdb.bands (band_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------
-- Insert admin data
-- -----------------------------------

INSERT INTO maintenance VALUES
(null, "breznar", "BreznarGeslo123", "admin"),
(null, "rozman", "RozmanGeslo123", "admin"),
(null, "lah", "LahGeslo123", "admin"),
(null, "hercog", "HercogGeslo123", "admin"),
(null, "simunic", "SimunicGeslo123", "admin");

-- -----------------------------------
-- Insert test data
-- -----------------------------------

INSERT INTO countrys VALUES (null, "NullCountry");

INSERT INTO countrys VALUES (null, "Slovenija");

INSERT INTO citys VALUES (null, "NullCity");

INSERT INTO citys VALUES (null, "Maribor");

INSERT INTO addresses VALUES (null, 1, 1, "NullAddress");

INSERT INTO addresses VALUES (null, 2, 2, "Koroška cesta 18");

INSERT INTO types_of_bands VALUES
(null, "NullType");

INSERT INTO types_of_bands VALUES
(null, "Narodno zabavno");

INSERT INTO bands VALUES (null, 1, 1, "NullBand", 0, "NullBand");

INSERT INTO bands VALUES (null, 2, 2, "Testna skupina 1", 0, "Samo testna skupina za testiranje skupine");

INSERT INTO users VALUES (null, 2, "Cene", "Veseljak", "cene.veseljak@test.com", "GesloTest123", 0, "VarnostniToken");

INSERT INTO artists VALUES 
(null, "Vlado Kreslin"),
(null, "Jan Plestenjak");

INSERT INTO types_of_songs VALUES (null, "Narodno zabavna glasba");

INSERT INTO songs VALUES (null, "Testna muzika 1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", 1, null, 1);

INSERT INTO songs VALUES (null, "Testna muzika 2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", 1, null, 1);

INSERT INTO types_of_songs_has_songs VALUES
(null, 1, 1),
(null, 1, 2);

INSERT INTO artists_has_songs VALUES 
(null, 1, 1),
(null, 2, 2);

INSERT INTO band_songs VALUES
(null, 1, 1, "Neka oglasitev"),
(null, 2, 2, "Neka oglasitev");

INSERT INTO band_has_songs VALUES 
(null, 1, 2),
(null, 2, 2);

INSERT INTO practices VALUES
(null, 2, 2, "2020-01-01", "14:00:00");

INSERT INTO practices_songs VALUES
(null, 1, 1),
(null, 2, 1);

INSERT INTO perfomances VALUES
(null, 2, 2, "2019-12-10", "18:00:00", "Nastop na glavnem trgu");

ALTER DATABASE glasbeniasistentdb CHARACTER SET utf8 COLLATE utf8_general_ci;